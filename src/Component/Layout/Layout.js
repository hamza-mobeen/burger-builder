import React from 'react';
import Auxiliary from '../../HOC/Auxiliary';
import classes from './Layout.css';

const layout = (props) => (
<Auxiliary>
    <div>Layout , Sidebar</div>
    <main className= {classes.Content}> 
        {props.children}
    </main>
</Auxiliary>

);

export default layout;