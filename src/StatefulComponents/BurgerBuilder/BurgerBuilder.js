import React, {Component} from 'react';
import Auxiliary from '../../HOC/Auxiliary';
import Burger from '../../Component/Burger/Burger';


class BurgerBuilder extends Component{
    state={
        ingredients : { 

            salad: 1,
            bacon: 1,
            cheese: 2,
            meat: 2
          }
    }
    render(){
        return (

            <Auxiliary>
                <Burger ingredients={this.state.ingredients} />
                <div>Build Controls</div>
            </Auxiliary>
        );
    }
}

export default BurgerBuilder;